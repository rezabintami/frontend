import { createStore } from 'redux'

// tambah/modif initial state untuk app reactjs ini disini
const emptyUser = {
    id: "",
    name: "",
    email: "",
    emailverification: false,
    photo: null,
    gender: null,
    address: "",
    isapproved: false,
    isadmin: false,
    token: "",
}

const initialState = {
    userIsLoggedIn: false,
    user: emptyUser
}

// berbagai action type yang tersedia di app ini:
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                userIsLoggedIn: true,
                user: {
                    id: action.payload.user.id,
                    name: action.payload.user.name,
                    email: action.payload.user.email,
                    emailverification: action.payload.user.emailverification,
                    photo: action.payload.user.photo,
                    gender: action.payload.user.gender,
                    profession: action.payload.user.profession,
                    github: action.payload.user.github,
                    gitlab: action.payload.user.gitlab,
                    linkedin: action.payload.user.linkedin,
                    handphone: action.payload.user.handphone,
                    address: action.payload.user.address,
                    isapproved: action.payload.user.isapproved,
                    isadmin: action.payload.user.isadmin,
                    token: action.payload.user.token,
                }
            }
        case 'LOGOUT':
            return {
                ...state,
                userIsLoggedIn: false,
                user: emptyUser
            }
        case 'CHANGE_PHOTO':
            return {
                ...state,
                user: {
                    ...state.user,
                    photo: action.payload.photo,
                }
            }
        default:
            return state
    }
}

const store = createStore(
    reducer,
    // untuk debugger pada web browser
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store