import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dashboard from './Dashboard'
import './Home.css'
import HomeJumbotron from './HomeJumbotron'

class Home extends Component {
    render() {
        // jika sudah login, tampilkan dasbor
        return this.props.userIsLoggedIn? <Dashboard /> : <HomeJumbotron />
    }
}

function mapStateToProps(state) {
    return {
        userIsLoggedIn: state.userIsLoggedIn,
    }
}

export default connect(mapStateToProps, null)(Home)