import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import "./Error404.css"

export default class Error404 extends Component {
    render() {
        return (
            <div className="container m-auto text-center error">
                <h2 className="font-weight-bold">Page Not Found</h2>
                <p>Laman yang kamu cari tidak ditemukan</p>
                <Link to="/" role="button" className="btn btn-small btn-error-home">Home</Link>
            </div>
        )
    }
}
