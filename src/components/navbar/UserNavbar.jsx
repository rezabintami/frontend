import React, { Component, Fragment } from 'react'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PlusIcon from '../../assets/image/plus.png'
import EditIcon from '../../assets/image/edit.png'
import GroupIcon from '../../assets/image/group.png'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { reactLocalStorage } from 'reactjs-localstorage'

// Item pada navbar yang hanya dapat diakses user yang sudah log in
class UserNavbar extends Component {
    constructor(props) {
        super(props)
        this._handleLogout = this._handleLogout.bind(this)
    }

    _handleLogout() {
        // tell global state and clear loc storage
        this.props.dispatch({
            type: "LOGOUT"
        })
        reactLocalStorage.clear()
    }

    render() {
        return (
            <Fragment>
                <li className="nav-item dropdown">
                    <Link className="nav-link" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img alt="buat konten baru" src={PlusIcon} className="navbar-icon" /></Link>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <h6 class="dropdown-header">Buat</h6>
                        <Link to="/user/create-article" className="dropdown-item"><img className="navbar-icon" alt="buat artikel baru" src={EditIcon} /> Artikel</Link>
                        <Link className="dropdown-item"><img className="navbar-icon" alt="buat PI baru" src={GroupIcon} /> Proyek Inovasi</Link>
                    </div>
                </li>
                <li className="nav-item dropdown">
                    <Link className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><FontAwesomeIcon icon={faUser} /> {this.props.user.email}</Link>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        {(this.props.userIsLoggedIn && this.props.user.isadmin) &&
                            <Link to="/admin/approve-registrations" className="dropdown-item">Approve Registrations</Link>}
                        <Link to="/user/profile" className="dropdown-item">Profil</Link>
                        <Link role="button" onClick={this._handleLogout} className="dropdown-item btn btn-navbar-logout">Logout</Link>
                    </div>
                </li>
            </Fragment>
        )
    }
}

const _mapStatetoProps = state => {
    return {
        userIsLoggedIn: state.userIsLoggedIn,
        user: state.user
    }
}

const _mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(_mapStatetoProps, _mapDispatchToProps)(UserNavbar)