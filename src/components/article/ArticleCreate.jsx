import React, { Component } from 'react'
import ArticleCreateForm from '../article/ArticleCreateForm'

export default class ArticleCreate extends Component {
    render() {
        return (
            <div className="container">
                <ArticleCreateForm />
            </div>
        )
    }
}
