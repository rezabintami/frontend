import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import LoadingScreen from '../loading/index'
import humanDate from '../../helper/humanDate'
import LikeIcon from '../../assets/image/heart1.png'
import './Article.css'

// menampilkan sebuah artikel
class Article extends Component {
    constructor(props) {
        super(props)
        this.state = {
            article: {},
            articleFound: true,
            hasHitAPI: false,
            myComment: "",
        }

        this._handleComment = this._handleComment.bind(this)
        this._handleLike = this._handleLike.bind(this)
    }

    componentDidMount() {
        const { article_id } = this.props.match.params
        this._loadArticle(article_id)
    }

    _renderComments() {
        if (this.state.hasHitAPI && this.state.articleFound) {
            const comments = this.state.article.comment.map((item) => {
                return (
                    <div className="div-comment">
                        {item.comment}<br />
                        <p className="text-right"><small>{humanDate(item.created_at)}</small></p>
                    </div>)
            })
            return comments
        }
    }

    async _handleLike() {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", this.props.user.token);
        myHeaders.append("Content-Type", "application/json")

        var raw = JSON.stringify({ "article_id": this.state.article.id, "like": true })

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        }

        await fetch("http://kelompok5a.dtstakelompok1.com/article/like", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
    }

    async _handleComment(event) {
        event.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", this.props.user.token);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({ "comment": this.state.myComment, "article_id": this.state.article.id })

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        const response = await fetch("http://kelompok5a.dtstakelompok1.com/article/create-comment", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            console.log('you commented successfully')
            this.setState({ myComment: "" })
        } else {
            console.log('Error: ', response.error)
            alert(`Error: ${response.error}`)
        }
    }

    async _loadArticle(id) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        const response = await fetch(`http://kelompok5a.dtstakelompok1.com/article/get-articles/${id}`, requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            });

        if (response == null) {
            this.setState({ articleFound: false })
            return null
        }

        if (response.success) {
            this.setState({
                article: response.articles,
                articleFound: true,
            })
            console.log(this.state.article.comment)
        } else {
            this.setState({ articleFound: false })
            console.log('Error: ', response.error)
            alert(`Error: ${response.error}`)
        }
        this.setState({ hasHitAPI: true })
    }

    render() {
        if (!this.state.hasHitAPI) return <LoadingScreen />
        if (!this.state.articleFound && this.state.hasHitAPI) return <Redirect to='/error' />
        return (
            <div className="container">
                <article className="article">
                    <h1>{this.state.article.title}</h1>
                    <div className="article-detail">
                        By <Link className="a-app" to={`/user/detail/${this.state.article.profile_id}`}><b>{this.state.article.author}</b></Link> on {humanDate(this.state.article.created_at)}
                        {parseInt(this.state.article.like) > 0 && <div><img className="img-like" alt="suka" src={LikeIcon} /> {this.state.article.like}</div>}
                    </div>
                    <div className="header-img-container text-center">
                        {this.state.article.photo && <img src={this.state.article.photo} className="article-photo-header" alt="header artikel" />}
                    </div>
                    <div className="article-body" dangerouslySetInnerHTML={{
                        __html: this.state.article.description
                    }}>
                    </div>
                    {this.props.userIsLoggedIn && (
                        <div className="div-like">
                            <button onClick={this._handleLike} className="btn btn-light"><img className="img-like" alt="suka" src={LikeIcon} /> Klik untuk suka</button>
                        </div>
                    )}
                </article>
                <div className="sharethis-inline-share-buttons"></div>
                <section className="comments px-3">
                    <h5>Komentar</h5>
                    {this.props.userIsLoggedIn &&
                        <form onSubmit={this._handleComment} className="form-comment">
                            <div className="form-group">
                                <textarea onChange={(e) => this.setState({ myComment: e.target.value })} className="form-control"></textarea>
                            </div>
                            <button onClick={this._handleComment} className="btn btn-app">Kirim komentar</button>
                        </form>}
                    <h6>{this.state.article.comment.length} komentar</h6>
                    {this._renderComments()}
                </section>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userIsLoggedIn: state.userIsLoggedIn,
        user: state.user,
    }
}

export default connect(mapStateToProps, null)(Article)