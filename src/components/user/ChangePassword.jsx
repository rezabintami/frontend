import React, { Component } from 'react'
import { connect } from 'react-redux'
import SimpleModal from '../modal/SimpleModal'

class ChangePassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            oldPassword: "",
            newPassword: "",
            newPassword2: "",
        }
        this._handlePasswordChange = this._handlePasswordChange.bind(this)
    }

    async _handlePasswordChange(event) {
        event.preventDefault()

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", this.props.user.token);

        var raw = JSON.stringify({ "password": this.state.oldPassword, "new_password": this.state.newPassword });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        const response = await fetch("http://kelompok5.dtstakelompok1.com/account/changepassword", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            //console.log('success: ', response)
            window.$('#passwordUpdateModal').modal('show')
        } else {
            console.log('Error: ', response.error)
            alert(`Error: ${response.error}`)
        }
    }

    render() {
        return (
            <form onSubmit={this._handlePasswordChange} className="form-profile">
                <div class="form-group">
                    <label htmlFor="id-old-password">Password saat ini</label>
                    <input onChange={(e) => { this.setState({ oldPassword: e.target.value }) }} type="password" class="form-control" id="id-old-password" />
                </div>
                <div class="form-group">
                    <label htmlFor="id-new-password">Password baru</label>
                    <input onChange={(e) => { this.setState({ newPassword: e.target.value }) }} type="password" class="form-control" id="id-new-password" />
                </div>
                <div class="form-group">
                    <label htmlFor="id-new-password2">Ulangi password baru</label>
                    <input onChange={(e) => { this.setState({ newPassword2: e.target.value }) }} type="password" class="form-control" id="id-new-password2" />
                </div>
                <button onClick={this._handlePasswordChange} disabled={(this.state.newPassword !== this.state.newPassword2) || this.state.oldPassword.length === 0 || this.state.newPassword.length === 0} className="btn btn-app mr-2">Simpan</button>

                <SimpleModal modalID='passwordUpdateModal' modalTitle="Sukses" modalBody="Password Anda berhasil diperbarui" />
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, null)(ChangePassword)