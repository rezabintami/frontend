import React, { Component } from 'react'
import { connect } from 'react-redux'
import SimpleModal from '../modal/SimpleModal'

class UpdateProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: this.props.user.name,
            email: this.props.user.email,
            gender: this.props.user.gender,
            profession: this.props.user.profession,
            github: this.props.user.github,
            gitlab: this.props.user.gitlab,
            linkedin: this.props.user.linkedin,
            handphone: this.props.user.handphone,
            address: this.props.user.address,
        }

        this._handleGenderChange = this._handleGenderChange.bind(this)
        this._handleProfileChange = this._handleProfileChange.bind(this)
    }

    _handleGenderChange(event) {
        this.setState({ gender: event.target.value })
    }

    async _handleProfileChange(event) {
        event.preventDefault()

        var myHeaders = new Headers()
        myHeaders.append("Content-Type", "application/json")
        myHeaders.append("Authorization", this.props.user.token)

        var raw = JSON.stringify({
            "name": this.state.name,
            "gender": this.state.gender,
            "profession": this.state.profession,
            "github": this.state.github,
            "gitlab": this.state.gitlab,
            "linkedin": this.state.linkedin,
            "handphone": this.state.handphone,
            "address": this.state.address,
        })

        const response = await fetch("http://kelompok5.dtstakelompok1.com/account/update", {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: "follow"
        })
            .then(response => response.json())
            .catch(error => console.log('Error: ', error))
        if (response == null) return null
        if (response.success) {
            console.log(response.account)
            // update state user via dispatch
            this.props.dispatch({
                type: "LOGIN",
                payload: {
                    user: {
                        id: response.account.id,
                        name: response.account.name,
                        email: response.account.email,
                        emailverification: response.account.emailverification,
                        photo: response.account.photo,
                        gender: response.account.gender,
                        profession: response.account.profession,
                        github: response.account.github,
                        gitlab: response.account.gitlab,
                        linkedin: response.account.linkedin,
                        handphone: response.account.handphone,
                        address: response.account.address,
                        isapproved: response.account.isapproved,
                        isadmin: response.account.isadmin,
                        token: this.props.user.token,
                    }
                }
            })
            window.$('#profileUpdateModal').modal('show')
        } else {
            alert(`Error: ${response.error}`)
        }
    }

    render() {
        return (
            <form onSubmit={this._handleProfileChange} className="form-profile">
                <div className="form-group">
                    <label htmlFor="input-name">Nama:</label>
                    <input onChange={(e) => this.setState({ name: e.target.value })} type="text" className="form-control" value={this.state.name} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-email">Email:</label>
                    <input type="email" className="form-control" value={this.state.email} id="input-email" disabled />
                </div>
                <div className="my-2">
                    Jenis kelamin:
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="gender" id="radio-laki-laki" value="Laki-laki" checked={this.state.gender === 'Laki-laki'} onChange={this._handleGenderChange} />
                        <label className="form-check-label" htmlFor="radio-laki-laki">Laki-laki</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="gender" id="radio-perempuan" value="Perempuan" checked={this.state.gender === 'Perempuan'} onChange={this._handleGenderChange} />
                        <label className="form-check-label" htmlFor="radio-perempuan">Perempuan</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="gender" id="radio-lainnya" value="Lainnya" checked={this.state.gender === 'Lainnya'} onChange={this._handleGenderChange} />
                        <label className="form-check-label" htmlFor="radio-lainnya">Lainnya</label>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Pekerjaan:</label>
                    <input onChange={(e) => this.setState({ profession: e.target.value })} type="text" className="form-control" value={this.state.profession} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Akun Gitlab:</label>
                    <input onChange={(e) => this.setState({ gitlab: e.target.value })} type="text" className="form-control" value={this.state.gitlab} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Akun Github:</label>
                    <input onChange={(e) => this.setState({ github: e.target.value })} type="text" className="form-control" value={this.state.github} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Akun LinkedIn:</label>
                    <input onChange={(e) => this.setState({ linkedin: e.target.value })} type="text" className="form-control" value={this.state.linkedin} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Nomor Telepon:</label>
                    <input onChange={(e) => this.setState({ handphone: e.target.value })} type="text" className="form-control" value={this.state.handphone} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-alamat">Alamat:</label>
                    <textarea onChange={(e) => this.setState({ address: e.target.value })} className="form-control" value={this.state.address} id="input-alamat" rows="5"></textarea>
                </div>
                <button className="btn btn-app mr-2" onClick={this._handleProfileChange}>Simpan</button>

                <SimpleModal modalID='profileUpdateModal' modalTitle="Sukses" modalBody="Profil Anda berhasil diperbarui" />
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile)