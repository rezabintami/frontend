import React, { Component } from 'react'
import './index.css'
import CoCreateLogo from '../../assets/image/logo512.png'
import './index.css'

export default class SplashScreen extends Component {
    render() {
        return (
            <div className="splashscreen my-auto text-center">
                <img className="img-splashscreen" src={CoCreateLogo} alt="logo loading" />
                <h3>Mohon tunggu</h3>
                <div className="loading-dot">.</div>
            </div>
        )
    }
}