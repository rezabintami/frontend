# CoCreate

## Deskripsi
CoCreate merupakan platform digital komunitas bagi tim banking. Platform ini menyediakan berbagai fitur diantaranya:

### **Kolaborasi CoCreate**
Anggota CoCreate dapat membuat artikel baru tentang _new idea/insight_, berita, bahkan proyek inovasi dengan kategori tertentu dengan membuat grup Proyek Inovasi (PI).

### **Portfolio Anggota CoCreate**
Anggota CoCreate dapat melihat kontribusi/portfolio anggota lain, menambahkan dan mengubah daftar proyek external, pengetahuan/edukasi/expertise/knowledges, dan pengalaman di portfolio mereka, serta mengunduh portofolio sebagai CV/Resume digital.

### **CoCreate Innovation Showcases**
Proyek inovasi yang telah dibuat bisa dipamerkan menggunakan fitur CoCreate Innovation Showcases.

## Team Members
#### DTS ITP UI - Kelas 1 Kelompok 5
##### Front-End Team:
- Aisyah Nurul Hidayah 
- Gottfried Christophorus Prasetyadi 
- Rian Apriansyah 
- Muh Aswar Bakri

###### Back-End Team:
- Muhammad Reza Bintami
- Muhammad Zikri
- Jupri Eka Pratama
- Esteria Novebri Simanjuntak